import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {

  public gnomesListing: Array<GnomeInterface>;
  constructor(private http: HttpClient) {}

  getDataFromApi() {
    const URL_API = 'https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json';
    return this.http.get<GnomeInterface>(URL_API);
  }
}

export interface GnomeInterface {
  id: string;
  name: string;
  thumbnail: string;
  age: number;
  weight: string;
  height: string;
  hair_color: string;
  professions: Array<string>;
  friends: Array<string>;
}