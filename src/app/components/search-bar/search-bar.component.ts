import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService, GnomeInterface } from '../../services/data.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html'
})

export class SearchBarComponent implements OnInit {

  constructor( private router:Router ) {}

  ngOnInit(): void {
  }

  navigateToSearchGnome( term: string ) {
    if (term.length > 0 ) {
      this.router.navigate(['/filter', term]);
    }
  }

}
