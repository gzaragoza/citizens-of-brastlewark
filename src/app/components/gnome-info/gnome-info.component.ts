import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-gnome-info',
  templateUrl: './gnome-info.component.html'
})

export class GnomeInfoComponent implements OnInit {
  idCharacter: number;
  gnome: any = {
    id: '',
    name: '',
    thumbnail: '',
    age: '',
    weight: '',
    height: '',
    hair_color: '',
    professions: '',
    friends: ''
  };

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getCharacters();
  }

  getParam() {
    this.route.params
      .subscribe((params: Params) => {
        this.idCharacter = + params['id'];
        this.gnome = this.getCharacter(this.idCharacter);
      });
    }
    getCharacters() {
      this.dataService.getDataFromApi().subscribe((data: any) => {
        this.gnome = data.Brastlewark;
        this.getParam();
      }, (error) => console.log('Error ${error}'));
    }
    getCharacter(index: number) {
      return this.gnome[index];
  }
}
