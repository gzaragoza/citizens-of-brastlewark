import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService, GnomeInterface } from '../../services/data.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html'
})

export class FilterComponent implements OnInit {

  gnomes: any[] = [];
  genomesArr: GnomeInterface[] = [];
  public page = '';
  public gnomesListing: Array<GnomeInterface>;

  dataNames: any = {
      id: '',
      name: '',
      thumbnail: '',
      age: '',
      weight: '',
      height: '',
      hair_color: '',
      professions: '',
      friends: ''
  };

  constructor( private activatedRoute: ActivatedRoute,
               private dataService: DataService
               ) {}

  ngOnInit(): void {
    this.getGnomes();
  }

  getGnomes() {
    this.dataService.getDataFromApi()
    .subscribe(data => this.successData(data), err => this.failed(err));
  }

  successData(data) {
    this.gnomesListing = data.Brastlewark;

    this.activatedRoute.params.subscribe( params => {
      if( params['term'] !== undefined) {
        this.searchGnomes( params['term'] );
      }
    });

  }
  failed(err) {
    console.log(err);
  }
  
  searchGnomes( term: string) {
    this.genomesArr = [];
    term = term.toLowerCase();
    for ( let i = 0; i < this.gnomesListing.length; i ++ ) {

      const gnomes = this.gnomesListing[i];
      const name = gnomes.name.toLowerCase();

      if ( name.indexOf( term ) >= 0 ) {
        this.genomesArr.push(gnomes);
      }
    }
    return this.genomesArr;
  }

}
