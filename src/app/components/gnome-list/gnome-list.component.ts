
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService, GnomeInterface } from '../../services/data.service';

@Component({
  selector: 'app-gnome-list',
  templateUrl: './gnome-list.component.html'
})
export class GnomeListComponent implements OnInit {

  public gnomesListing: Array<GnomeInterface>;
  public page = '';
  gnomes: any = {};

  constructor(private dataService: DataService,
              private activateRoute: ActivatedRoute) {}

  ngOnInit() {
    this.getGnomes();
  }

  getGnomes() {
    this.dataService.getDataFromApi()
    .subscribe((data: any) =>
      this.gnomesListing = data.Brastlewark,
      error => console.log(error));
  }
}
