
import { Routes, RouterModule } from '@angular/router';
import { GnomeListComponent } from './components/gnome-list/gnome-list.component';
import { GnomeInfoComponent } from './components/gnome-info/gnome-info.component';
import { FilterComponent } from './components/filter/filter.component';


const appRoutes: Routes = [
    { path: 'gnome/:id', component: GnomeInfoComponent },
    { path: 'filter/:term', component: FilterComponent },
    { path: 'gnomes', component: GnomeListComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'gnomes' }
];

export const AppRoutingModule = RouterModule.forRoot(appRoutes);